package com.example.lenovo.tienda;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import static com.android.volley.VolleyLog.TAG;

public class AdaptadorDatos extends RecyclerView.Adapter<AdaptadorDatos.ViewHolderDatos> {
    RequestOptions options ;
    private Context mContext;
   List<app> listaDatos;
    private Context context;


    public AdaptadorDatos(List<app> lista , Context context){

        this.listaDatos=lista;
        this.context=context;
        //options = new RequestOptions()
          //      .centerCrop()
            //    .placeholder(R.drawable.ic_menu_camera)
              //  .error(R.drawable.ic_menu_share);
    }
    @Override
    //enlasamos el adaptador con el itenlista los contenidos del carrosel
    public AdaptadorDatos.ViewHolderDatos onCreateViewHolder(final ViewGroup parent, int viewType) {



        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlist,parent,false);
        RecyclerView.LayoutParams layoutParams=new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
       view.setLayoutParams(layoutParams);
         final ViewHolderDatos date=new ViewHolderDatos(view);
            date.elconteiner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(parent.getContext(), infoapp.class);
                // sending data process
                i.putExtra("titulo",listaDatos.get(date.getAdapterPosition()).getNombre());
                i.putExtra("id",listaDatos.get(date.getAdapterPosition()).getDesarrolladores());

                i.putExtra("icono",URL_icono+listaDatos.get(date.getAdapterPosition()).getFoto());
               //0 i.putExtra("video_aplicacion",URL_video+listaDatos[date.getAdapterPosition()].getv)

                parent.getContext().startActivity(i);
            }
        });

        return  date;


        }
   // private static final String URL_video = "http://181.215.121.80/appshop/public/iconos/";
    private static final String URL_icono = "http://181.215.121.80/appshop/public/iconos/";

    @Override
    public void onBindViewHolder(final AdaptadorDatos.ViewHolderDatos holder, final int position) {
        holder.lafoto.setImageUrl(URL_icono+listaDatos.get(position).getFoto(), VolleySingleton.getInstance(context).getImageLoader());
    holder.elnombre.setText(listaDatos.get(position).getNombre());
    holder.eldesarrolador.setText(listaDatos.get(position).getDesarrolladores());


    //holder.lafoto.setText(listaDatos[position].getFoto());
       // Glide.with(context).load(listaDatos.get(position).getFoto()).apply(options).into(holder.lafoto);

      /*  ImageRequest request = new ImageRequest(listaDatos.get(position).getFoto(),
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        holder.lafoto.setImageBitmap(bitmap);
                        Log.e("se esta ejecutando",listaDatos.get(position).getFoto());
                    }
                }, 0, 0, null,null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                       // holder.lafoto.setImageResource(R.drawable.error);
                        Log.d(TAG, "Error en respuesta Bitmap: "+ error.getMessage());
                    }
                });*/


    }

    @Override
    public int getItemCount() {

        return listaDatos.size();
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {

        TextView elnombre, eldesarrolador;
        NetworkImageView lafoto;
        LinearLayout elconteiner;


        public ViewHolderDatos(View itemView) {
            super(itemView);
            elconteiner =(LinearLayout) itemView.findViewById(R.id.conteiner);
            elnombre = (TextView) itemView.findViewById(R.id.nombre);
            eldesarrolador = (TextView) itemView.findViewById(R.id.desarrollado);
            lafoto = (NetworkImageView) itemView.findViewById(R.id.imgicoApp);

        }




    }
    public void  busquedadatos(List<app> newList){
        listaDatos=new ArrayList<>();
        listaDatos.addAll(newList);
        notifyDataSetChanged();
    }
    }

