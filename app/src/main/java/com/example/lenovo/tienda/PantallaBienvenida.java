package com.example.lenovo.tienda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PantallaBienvenida extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_bienvenida);
        //despues de cargar aplicacion inicia la actividad principal y finlisa la actual
        startActivity(new Intent(PantallaBienvenida.this,Login.class));
        finish();
    }
}
