package com.example.lenovo.tienda;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by USUARIO on 30/07/2017.
 */

public class registro extends AppCompatActivity {

    RequestQueue colapeticiones;
    ArrayAdapter adapterapps,adapterjuegos;

    JSONObject apps;
    public static int segundos=8;
    public static int milisegundos=segundos*1000;
    public static int delay=2;
    TextView nombre,apellido,mail,username,contraseña;
    String nombrTe,apellidoT,mailT,usernameT,contraseñaT;

    ProgressBar barracargando;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        colapeticiones= Volley.newRequestQueue(this);

        nombre=(TextView)findViewById(R.id.nombre);
        nombrTe=nombre.getText().toString();
        apellido=(TextView)findViewById(R.id.apellido);
        apellidoT=apellido.getText().toString();
        mail=(TextView)findViewById(R.id.mail);
        mailT=mail.getText().toString();
        username=(TextView)findViewById(R.id.username);
        usernameT=username.getText().toString();
        contraseña=(TextView)findViewById(R.id.contrasena);
        contraseñaT=contraseña.getText().toString();
        final   RequestQueue requestQueue= Volley.newRequestQueue(this);
        findViewById(R.id.registrarse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String URLap="http://168.62.42.172/ciudadmusical/";


                StringRequest stringRequest =new StringRequest(Request.Method.POST, URLap, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            JSONObject obj = new JSONObject(s);
                            //String token=obj.getString("token");
                        } catch (JSONException e) {
                            System.out.println("couldn't parse string to jsonObject");

                        }
                    }}, new Response.ErrorListener(){

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<String, String>();

                        params.put("nombre", nombrTe.trim());
                        params.put("apellido", apellidoT.trim());
                        params.put("mail", mailT.trim());
                        params.put("username", usernameT.trim());
                        params.put("contraseña", contraseñaT.trim());
                        params.put("grant_type", "contraseña");
                        return params;
                    }
                };


                requestQueue.add(stringRequest);

            }
        });

    }}

