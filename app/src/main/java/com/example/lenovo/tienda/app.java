package com.example.lenovo.tienda;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

public class app {
    private String nombre;
    private String desarrolladores;

    private String foto;
    private String video;

    public app() {

    }

    public app(String nombre , String desarrolladores,String foto,String video) {
        this.desarrolladores=desarrolladores;

        this.nombre=nombre;
        this.foto=foto;
        this.video=video;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getDesarrolladores() {
        return desarrolladores;
    }

    public void setDesarrolladores(String desarrolladores) {
        this.desarrolladores = desarrolladores;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
