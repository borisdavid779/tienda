package com.example.lenovo.tienda;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class infoapp extends AppCompatActivity {
    private long myDownloadReference;
    DownloadManager coladescarga;
    String apka,n;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infoapp);
        getSupportActionBar().hide();
        Button btndescargar = (Button) findViewById(R.id.downlaund);
        final Bundle nombre = getIntent().getExtras();
        final String nombreap = nombre.getString("titulos");
        String URL1 = "http://maquinavstore.cloudapp.net:90/appshop/aplicaciontodas/" + nombreap + "/";


        btndescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apka = nombre.getString("apk");
                n = nombre.getString("n");

                IniciarDescarga(apka, n);

            }
        });


        // Recieve data

        String name = getIntent().getExtras().getString("titulo");
        String description = getIntent().getExtras().getString("id");
        String image_info = getIntent().getExtras().getString("icono");

        // ini views
        CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsingtoolbar_id);
        collapsingToolbarLayout.setTitleEnabled(true);

        TextView info_nombre = findViewById(R.id.info_nombre);
        TextView info_id = findViewById(R.id.desarroladores__info);
        TextView info_categorie = findViewById(R.id.info_categoria);
        TextView tv_description = findViewById(R.id.aa_description);
        ImageView img = findViewById(R.id.aa_thumbnail);

        // setting values to each view

        info_nombre.setText(name);
        info_id.setText(description);


        // set image using Glide
        Glide.with(this).load(image_info).into(img);
    }
        public void IniciarDescarga(String enlace,String nomb)
        {

            coladescarga=(DownloadManager)getSystemService(Context.DOWNLOAD_SERVICE);
            Uri link=Uri.parse(enlace);
            DownloadManager.Request peticiondescarga = new DownloadManager.Request(link);
            peticiondescarga.setTitle(nomb);
            peticiondescarga.allowScanningByMediaScanner();
            peticiondescarga.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            peticiondescarga.setMimeType("apk");
            peticiondescarga.setDescription(enlace);
            // peticiondescarga.setDestinationInExternalFilesDir(MainActivity.this, Environment.DIRECTORY_DOWNLOADS, "Descargas");
            peticiondescarga.setDestinationInExternalFilesDir(infoapp.this, Environment.DIRECTORY_DOWNLOADS, "Aplicacion.apk");
            myDownloadReference=coladescarga.enqueue(peticiondescarga);
            Toast.makeText(infoapp.this, "Se empezara la Descarga", Toast.LENGTH_SHORT).show();


        }




}
