package com.example.lenovo.tienda;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.app.VoiceInteractor;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



public class PantallaPrincipal extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private RecyclerView recycler,recycler2,recycler3, recycler4;
    private static final String URL_js = "http://181.215.121.80/appshop/aplicaciontodas";
    android.support.v7.widget.SearchView searchView;
    private AdaptadorDatos adapter;
    private JsonArrayRequest ArrayRequest;
    RequestQueue requestQueue;
    private List<app> ListDatos = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_principal);
        recycler = (RecyclerView) findViewById(R.id.reciclerview);
        recycler2 = (RecyclerView) findViewById(R.id.reciclerview2);
        recycler3 = (RecyclerView) findViewById(R.id.reciclerview3);
        recycler4 = (RecyclerView) findViewById(R.id.reciclerview4);
        recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycler2.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycler3.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycler4.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        requestQueue = Volley.newRequestQueue(PantallaPrincipal.this);
        //recycler.setLayoutManager(new LinearLayoutManager (this));
        llenardatos();
        AdaptadorDatos adaptador=new AdaptadorDatos(ListDatos,this);
        recycler.setAdapter(adaptador);
        recycler2.setAdapter(adaptador);
        recycler3.setAdapter(adaptador);
        recycler4.setAdapter(adaptador);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void llenardatos() {


        ArrayRequest = new JsonArrayRequest(URL_js, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                JSONObject jsonObject = null;


                for (int i = 0 ; i<response.length();i++) {

                    //Toast.makeText(getApplicationContext(),String.valueOf(i),Toast.LENGTH_SHORT).show();

                    try {

                        jsonObject = response.getJSONObject(i);
                        app apk = new app();


                        apk.setNombre(jsonObject.getString("id"));
                        apk.setDesarrolladores(jsonObject.getString("titulo"));
                        apk.setFoto(jsonObject.getString("icono"));
                        //apk.setVideo(jsonObject.getString("video_aplicacion"));

                        //Toast.makeText(MainActivity.this,anime.toString(),Toast.LENGTH_SHORT).show();
                        ListDatos.add(apk);
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                //af5814d0c0

                Toast.makeText(PantallaPrincipal.this,"aplicaciones cargadas  "+String.valueOf(ListDatos.size()),Toast.LENGTH_SHORT).show();
//                Toast.makeText(PantallaPrincipal.this,ListDatos.get(1).toString(),Toast.LENGTH_SHORT).show();

                //setRvadapter(ListDatos);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        requestQueue = Volley.newRequestQueue(PantallaPrincipal.this);
        requestQueue.add(ArrayRequest);
    }


    /*public void setRvadapter(List<app> lista) {

        AdaptadorDatos myAdapter = new AdaptadorDatos(lista, this);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(myAdapter);


    }


       /*final ProgressDialog progressDialog =new ProgressDialog(this);
        progressDialog.setMessage("cargando datos");
        progressDialog.show();
        StringRequest stringRequest=new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject =new JSONObject(s);
                    JSONArray array=jsonObject.getJSONArray("id");

                    for (int i=0;i<array.length();i++){
                        JSONObject o=array.getJSONObject(i);
                        app item =new app(o.getString("id"),o.getString("titulo"),o.getString("icono"));
                    listDatos.add(item);
                    }  adapter= new AdaptadorDatos(listDatos,getApplicationContext());
                    recycler.setAdapter(adapter);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),volleyError.getMessage(),Toast.LENGTH_LONG).show();

            }
        });RequestQueue requestQueue=Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

        /*colapeticiones= Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest peticionapss=new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {
                        Log.e("Exito al recibir datos", jsonArray.toString());
                        listDatos=llenardatosv2(jsonArray);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            Log.e("error de petticion --->",volleyError.getMessage());
            }
        });colapeticiones.add(peticionapss);




    }
    @SuppressLint("LongLogTag")
    public  ArrayList<app> llenardatosv2(JSONArra
    y jsonapps) {
        for (int i = 0; jsonapps.length() > i; i++) {
            try {
                JSONObject jsonapp = jsonapps.getJSONObject(i);
                app aplicacion = new app(jsonapp.getString("titulo"),jsonapp.getString("id"), jsonapp.getString("icono")
                );
                listacargados.add(aplicacion);
            } catch (JSONException e) {
                Log.e("Error de Insercion de datos", e.getMessage());
            }

        }
        return  listacargados;
    }*/

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pantalla_principal, menu);
       /* SearchManager searchManager=(SearchManager)getSystemService(Context.SEARCH_SERVICE);
        searchView = (android.support.v7.widget.SearchView)menu.findItem(R.id.Buscador).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint(getString(R.string.app_name));
        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                 newText =newText.toLowerCase();
                List<app> myList=new ArrayList<>();
                for (app nomb : ListDatos)
                {
                    String nombre =nomb.getNombre().toLowerCase();
                    if (nombre.contains(newText)){
                        myList.add(nomb);
                    }
                }
               adapter.busquedadatos(myList);
                return false;
            }
        });*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (id == R.id.nav_camera) {

        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {


        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }





}

